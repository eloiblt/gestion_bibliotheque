﻿import java.util.InputMismatchException;
import java.util.Scanner;

public class Test
{
    public static void main(String[] args) throws ChoixHorsMenuException, NbNegatifException,DoubleObjectException,NoneExistException,ErrorIndiceException,ErrorSeuilException
    {        
        Scanner input = new Scanner(System.in);
        
        CatalogueBibliotheque listeDoc = new CatalogueBibliotheque();
        Livre doc1 = new Livre("xfcg6","Le premier jour","Patrick LeUn",2001,"a",45,"ea");
        CD doc2 = new CD("fsdfsd5","Le deuxieme jour ","Patrick LeDeux",2002,"a");
        listeDoc.ajouterDocument(doc1);
        listeDoc.ajouterDocument(doc2);
        ListeMembre listeMembre = new ListeMembre();
        MembreBibliotheque memb1 = new MembreEtudiant("jacques", "lefaivre", "0782452623","6 rue napoleon");
        MembreBibliotheque memb2 = new MembrePersonnel("bruno", "levasseur", "0645965632","34 boulevard des champs elysées");
        listeMembre.ajouterMembre(memb1);
        listeMembre.ajouterMembre(memb2);
        ListeEmploye listeemp = new ListeEmploye();
        EmployeBibliotheque emp1 = new EmployeBibliotheque("xcrd","eloi");
        listeemp.ajouterEmploye(emp1);
        
        boolean menu = true;
        boolean retour;

        int choix = 0;
        while (menu)
        {
            affichageMenu();

            while (true)
            {
                try
                {
                    System.out.print("Que voulez vous faire ? : ");
                    choix = input.nextInt();
                    input.nextLine();
                    if(choix < 1 || choix > 15)
                    {
                        throw new ChoixHorsMenuException(choix);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            } 

            switch(choix)
            {
                case 1: retour = ajoutDocCatalogue(input, listeDoc);
                    if(retour == false)
                        System.out.println("erreur lors de l'ajout du doc");
                    break;
                case 2: retour = ajouterMembreCatalogue(input, listeMembre, listeemp);
                    if(retour == false)
                        System.out.println("erreur lors de l'ajout du membre");
                    break;
                case 3: retour = supprimerDocCatalogue(input, listeDoc);
                    if(retour == false)
                        System.out.println("erreur lors de la suppression");
                    break;
                case 4: retour = supprimerMembreCatalogue(input, listeMembre, listeemp);
                    if (retour == false)
                        System.out.println("erreur lors de la suppression");
                    break;
                case 5: retour = emprunterDocument(input, listeDoc, listeMembre, listeemp);
                    if (retour == false)
                        System.out.println("erreur lors de l'emprunt du document ");
                    break;
                case 6: retour = retournerDocument(input, listeDoc);
                    if (retour == false)
                        System.out.println("erreur lors du retour du document ");
                    break;
                case 7: retour = reserverDocument(input,listeDoc, listeMembre, listeemp);
                    if (retour == false)
                        System.out.println("erreur lors de la reservation du document ");
                    break;
                case 8: retour = annulerReservationDocument(input, listeDoc, listeMembre, listeemp);
                    if (retour == false)
                        System.out.println("erreur lors de la reservation du document ");
                    break;
                case 9: afficherInfosDoc ( input, listeDoc);
                    break;
                case 10: afficherInfosMembre (input, listeMembre, listeemp);
                    break;
                case 11: afficherlisteDoc(listeDoc);
                    break;
                case 12: afficherlisteMembre(listeMembre);
                    break;
                case 13: System.out.println("Nombre de livres : " + listeDoc.compteLivres());
                    break;
                case 14: System.out.println("Nombre de CD : " + listeDoc.compteCDs());
                    break;
                case 15: System.out.println("Au revoir !");
                    menu = false;
                    break;
                default : System.out.println("Mauvais choix dans le menu");    
            }   
        }
    }
    
    public static void affichageMenu ()
    {
        System.out.println("\n");
    	System.out.println("1-- Ajouter un document");
        System.out.println("2-- Ajouter un membre");
        System.out.println("3-- Supprimer un document");
        System.out.println("4-- Supprimer une personne");
        System.out.println("5-- Emprunter un document");
        System.out.println("6-- Retourner un document");
        System.out.println("7-- Reserver un document");
        System.out.println("8-- Annuler la reservation d'un document");
        System.out.println("9-- Informations sur un document");
        System.out.println("10-- Informations sur un membre");
        System.out.println("11-- Afficher la liste des documents");
        System.out.println("12-- Afficher la liste des membres");
        System.out.println("13-- Afficher le nombre de livres presents");
        System.out.println("14-- Afficher le nombre de CD presents");
        System.out.println("15-- Quitter le menu");
    }

    public static boolean ajoutDocCatalogue(Scanner input, CatalogueBibliotheque listeDoc) throws DoubleObjectException 
    {
        System.out.print("Entrez le code d'archivage du document : ");
        String code = input.nextLine();        
        System.out.print("Entrez le titre du document : ");
        String titre = input.nextLine();
        System.out.print("Entrez l'auteur du document : ");
        String auteur = input.nextLine();

        int annee = 0;
        while(true)
        {
            try
            {
                System.out.print("Entrez l'annee du document : ");
                annee = input.nextInt();
                input.nextLine();
                if(annee < 0)
                {
                    throw new NbNegatifException();
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (NbNegatifException exception2)
            {
                continue;
            }
        }   

        boolean menu=true;
        while(menu)
        {
            int choix = 0;
            while (true)
            {
                try
                {
                    System.out.println("\n1-- Livre\n2-- CD\n3-- DocURL");
                    System.out.print("Quel est le type du document ? : ");
                    choix = input.nextInt();
                    input.nextLine();
                    if(choix < 1 || choix > 3)
                    {
                        throw new ChoixHorsMenuException(choix);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            } 

            switch(choix)
            {
                case 1: 
                    System.out.print("Entrez l'editeur du livre : ");
                    String editeur = input.nextLine();

                    int nbPages = 0;
                    while (true)
                    {
                        try
                        {
                            System.out.print("Entrez le nombre de pages du livre : ");
                            nbPages = input.nextInt();
                            input.nextLine();
                            if(nbPages < 0)
                            {
                                throw new NbNegatifException();
                            }
                            System.out.println("\n");
                            break;
                        }
                        catch (InputMismatchException exception)
                        {
                            input.nextLine();
                            System.out.println("\nSaisie Invalide ! \n");
                            continue;
                        }
                        catch (NbNegatifException exception2)
                        {
                            continue;
                        }
                    } 
                    
                    System.out.print("Entrez l'IBSN du livre : ");
                    String isbn = input.nextLine();
                    return listeDoc.ajouterDocument(new Livre(code,titre,auteur,annee,editeur,nbPages,isbn));
                case 2:
                    System.out.print("Entrez la liste de morceaux du CD : ");
                    String morceaux = input.nextLine();
                    return listeDoc.ajouterDocument(new CD(code, titre, auteur, annee, morceaux));
                case 3: 
                    System.out.print("Entrez l'url du document : ");
                    String url = input.nextLine();
                    System.out.print("Entrez la description du document : ");
                    String description = input.nextLine();
                    System.out.print("Entrez l'entreprise auteure du document : ");
                    String ent = input.nextLine();
                    return listeDoc.ajouterDocument(new DocURL(code, titre, auteur, annee, url, description, ent));
            }
        }
        return false;
    }

    public static boolean ajouterMembreCatalogue(Scanner input, ListeMembre listeMembre, ListeEmploye listeemp) throws DoubleObjectException
    {   
        int choix;
        
        while (true)
        {
            try
            {
                System.out.print("\n1-- Membre Etudiant\n2-- Membre Personnel\n3 -- Employe Bibliotheque\nVotre choix ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 1 || choix > 3)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        String nom="", prenom="", tel="", ads="", id="";
        if(choix == 1 && choix == 2)
        {
            System.out.print("Entrez le nom du membre : ");
            nom = input.nextLine();
            System.out.print("Entrez le prenom du membre : ");
            prenom = input.nextLine();
            System.out.print("Entrez le tel du membre : ");
            tel = input.nextLine();
            System.out.print("Entrez l'adresse du membre : ");
            ads = input.nextLine();
        }
        else 
        {
            System.out.println("Entrez l'identification de l'employe");
            id = input.nextLine();
            System.out.println("Entrez le nom de l'employe");
            nom = input.nextLine();
        }

        if (choix == 1)
            return listeMembre.ajouterMembre(new MembreEtudiant(nom, prenom, tel, ads));
        else if(choix == 2)
            return listeMembre.ajouterMembre(new MembrePersonnel(nom, prenom, tel, ads));
        else 
            return listeemp.ajouterEmploye(new EmployeBibliotheque(id, nom));
    }

    public static boolean supprimerDocCatalogue(Scanner input, CatalogueBibliotheque listeDoc) throws NoneExistException
    {
        int doc = 0;
        while (true)
        {
            try
            {
                afficherlisteDoc(listeDoc);
                System.out.print("Quel document voulez vous supprimer ? : ");
                doc = input.nextInt();
                input.nextLine();
                if(doc < 0 || doc > listeDoc.afficheTailleTab() - 1)
                {
                    throw new ChoixHorsMenuException(doc);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        return listeDoc.supprimerDocument(listeDoc.accesDocumentTab(doc));
    }

    public static boolean supprimerMembreCatalogue(Scanner input, ListeMembre listeMembre, ListeEmploye listeemp) throws DoubleObjectException, NoneExistException
    {
    	int choix = 0;
        while (true)
        {
            try
            {
                System.out.print("Voulez vous supprimer un :\n1-- Membre \n2-- Employe\nVotre choix ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 1 || choix > 2)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        if(choix == 1)
        {
        	int membre = 0;
            while (true)
            {
                try
                {
                    afficherlisteMembre(listeMembre);
                    System.out.print("Quel membre voulez vous supprimer ? : ");
                    membre = input.nextInt();
                    input.nextLine();
                    if(membre < 0 || membre > listeMembre.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(membre);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            
            return listeMembre.supprimerMembre(listeMembre.accesMembre(membre));
        }
        else 
        {
        	int employe = 0;
            while (true)
            {
                try
                {
                	afficherlisteEmploye(listeemp);
                    System.out.print("Quel employe voulez vous supprimer ? : ");
                    employe = input.nextInt();
                    input.nextLine();
                    if(employe < 0 || employe > listeemp.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(employe);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            return listeemp.supprimerEmploye(listeemp.accesEmploye(employe));
        }
        
        
    }

    public static boolean emprunterDocument(Scanner input, CatalogueBibliotheque listeDoc, ListeMembre listeMembre, ListeEmploye listeemp) throws NoneExistException, ErrorSeuilException, ErrorIndiceException
    {
        int choixDoc = 0;
        while (true)
        {
            try
            {
                afficherlisteDoc(listeDoc);
                System.out.print("Quel document voulez vous emprunter ? : ");
                choixDoc = input.nextInt();
                input.nextLine();
                if(choixDoc < 0 || choixDoc > listeDoc.afficheTailleTab() - 1)
                {
                    throw new ChoixHorsMenuException(choixDoc);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        int choix = 0;
        while (true)
        {
            try
            {
                System.out.print("Qui va l'emprunter ?\n1-- un membre \n2-- un employe\nVotre choix ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 1 || choix > 2)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        if(choix == 1)
        {
        	int membre = 0;
            while (true)
            {
                try
                {
                    afficherlisteMembre(listeMembre);
                    System.out.print("Quel membre voulez vous choisir? : ");
                    membre = input.nextInt();
                    input.nextLine();
                    if(membre < 0 || membre > listeMembre.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(membre);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            
            return listeDoc.emprunt(choixDoc, listeMembre.accesMembre(membre));
        }
        else 
        {
        	int employe = 0;
            while (true)
            {
                try
                {
                	afficherlisteEmploye(listeemp);
                    System.out.print("Quel employe voulez vous choisir ? : ");
                    employe = input.nextInt();
                    input.nextLine();
                    if(employe < 0 || employe > listeemp.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(employe);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            return listeDoc.emprunt(choixDoc, listeemp.accesEmploye(employe));
        }
    }

    public static boolean retournerDocument(Scanner input, CatalogueBibliotheque listeDoc) throws ErrorIndiceException
    {
        int choix = 0;
        while (true)
        {
            try
            {
                afficherlisteDoc(listeDoc);
                System.out.print("Quel document voulez vous retourner ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 0 || choix > listeDoc.afficheTailleTab() - 1)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        return listeDoc.retour(choix);
    }
    
    public static boolean reserverDocument(Scanner input, CatalogueBibliotheque listeDoc, ListeMembre listeMembre, ListeEmploye listeemp) throws NoneExistException, ErrorIndiceException
    {
        int choixDoc = 0;
        while (true)
        {
            try
            {
                afficherlisteDoc(listeDoc);
                System.out.print("Quel document voulez vous reserver ? : ");
                choixDoc = input.nextInt();
                input.nextLine();
                if(choixDoc < 0 || choixDoc > listeDoc.afficheTailleTab() - 1)
                {
                    throw new ChoixHorsMenuException(choixDoc);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }

        int choix = 0;
        while (true)
        {
            try
            {
                System.out.print("Qui va le reserver ?\n1-- un membre \n2-- un employe\nVotre choix ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 1 || choix > 2)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        if(choix == 1)
        {
        	int membre = 0;
            while (true)
            {
                try
                {
                    afficherlisteMembre(listeMembre);
                    System.out.print("Quel membre voulez vous choisir? : ");
                    membre = input.nextInt();
                    input.nextLine();
                    if(membre < 0 || membre > listeMembre.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(membre);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            
            return listeDoc.reservation(choixDoc, listeMembre.accesMembre(membre));
        }
        else 
        {
        	int employe = 0;
            while (true)
            {
                try
                {
                	afficherlisteEmploye(listeemp);
                    System.out.print("Quel employe voulez vous choisir ? : ");
                    employe = input.nextInt();
                    input.nextLine();
                    if(employe < 0 || employe > listeemp.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(employe);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            return listeDoc.reservation(choixDoc, listeemp.accesEmploye(employe));
        }
    }
    
    public static boolean annulerReservationDocument (Scanner input, CatalogueBibliotheque listeDoc, ListeMembre listeMembre, ListeEmploye listeemp) throws NoneExistException, ErrorIndiceException
    {
        int choixDoc = 0;
        while (true)
        {
            try
            {
                afficherlisteDoc(listeDoc);
                System.out.print("Quel document voulez vous annuler la reservation ? : ");
                choixDoc = input.nextInt();
                input.nextLine();
                if(choixDoc < 0 || choixDoc > listeDoc.afficheTailleTab() - 1)
                {
                    throw new ChoixHorsMenuException(choixDoc);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }

        int choix = 0;
        while (true)
        {
            try
            {
                System.out.print("Qui veut annuler la reservation ?\n1-- un membre \n2-- un employe\nVotre choix ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 1 || choix > 2)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        if(choix == 1)
        {
        	int membre = 0;
            while (true)
            {
                try
                {
                    afficherlisteMembre(listeMembre);
                    System.out.print("Quel membre voulez vous choisir? : ");
                    membre = input.nextInt();
                    input.nextLine();
                    if(membre < 0 || membre > listeMembre.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(membre);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            return listeDoc.annulerReservation(choixDoc, listeMembre.accesMembre(membre));
        }
        else 
        {
        	int employe = 0;
            while (true)
            {
                try
                {
                	afficherlisteEmploye(listeemp);
                    System.out.print("Quel employe voulez vous choisir ? : ");
                    employe = input.nextInt();
                    input.nextLine();
                    if(employe < 0 || employe > listeemp.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(employe);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            return listeDoc.annulerReservation(choixDoc, listeemp.accesEmploye(employe));
        }	
    }
    
    public static void afficherInfosDoc (Scanner input, CatalogueBibliotheque listeDoc) throws NoneExistException
    {
        int choix = 0;
        while (true)
        {
            try
            {
                afficherlisteDoc(listeDoc);
                System.out.print("Pour quel document souhaitez vous afficher les informations ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 0 || choix > listeDoc.afficheTailleTab() - 1)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
    	System.out.println(listeDoc.accesDocumentTab(choix));
    }
    
    public static void afficherInfosMembre (Scanner input, ListeMembre listeMembre, ListeEmploye listeemp) throws NoneExistException
    {
    	int choix = 0;
        while (true)
        {
            try
            {
                System.out.print("Pour quel type de personne voulez avoir des informations ?\n1-- un membre \n2-- un employe\nVotre choix ? : ");
                choix = input.nextInt();
                input.nextLine();
                if(choix < 1 || choix > 2)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
        
        if(choix == 1)
        {
        	int membre = 0;
            while (true)
            {
                try
                {
                    afficherlisteMembre(listeMembre);
                    System.out.print("Quel membre voulez vous choisir? : ");
                    membre = input.nextInt();
                    input.nextLine();
                    if(membre < 0 || membre > listeMembre.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(membre);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            
            System.out.println(listeMembre.accesMembre(membre));
        }
        else 
        {
        	int employe = 0;
            while (true)
            {
                try
                {
                	afficherlisteEmploye(listeemp);
                    System.out.print("Quel employe voulez vous choisir ? : ");
                    employe = input.nextInt();
                    input.nextLine();
                    if(employe < 0 || employe > listeemp.afficheTaille() - 1)
                    {
                        throw new ChoixHorsMenuException(employe);
                    }
                    System.out.println("\n");
                    break;
                }
                catch (InputMismatchException exception)
                {
                    input.nextLine();
                    System.out.println("\nSaisie Invalide ! \n");
                    continue;
                }
                catch (ChoixHorsMenuException exception2)
                {
                    continue;
                }
            }
            System.out.println(listeemp.accesEmploye(employe));
        }
        

    	
    }
    
    public static void afficherlisteDoc(CatalogueBibliotheque listeDoc)
    {
        listeDoc.informationsDocumentsTab();
    }

    public static void afficherlisteMembre(ListeMembre listeMembre)
    {
        listeMembre.informationsMembres();
    }
    
    public static void afficherlisteEmploye(ListeEmploye listeemp)
    {
    	listeemp.informationsEmploye();
    }
}
