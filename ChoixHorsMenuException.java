public class ChoixHorsMenuException extends Exception
{
    public ChoixHorsMenuException(int nombre)
    {
        System.out.println("");
        System.out.println("Le nombre " + nombre + " que vous avez saisi ne correspond pas a un indice de menu ! ");
        System.out.println("");
    }
}