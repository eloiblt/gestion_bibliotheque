public class NoneExistException extends Exception
{
	public NoneExistException (Object o)
	{
		System.out.println("");
        System.out.println("L'objet de type " + o.getClass() + "est n'est pas present dans la liste, il ne peux donc pas etre supprime ou traite");
        System.out.println("");
	}

	public NoneExistException ()
	{
		System.out.println("");
        System.out.println("L'objet n'est pas present dans la liste, il ne peux donc pas etre supprime ou traite");
        System.out.println("");
	}
}
