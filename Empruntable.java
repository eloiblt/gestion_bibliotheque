public interface Empruntable
{
    public boolean emprunt(Notifiable emprunteur);

    public boolean retour();

    public boolean reservation(Notifiable reserveur);

    public boolean annulerReservation(Notifiable membre);
    
    public boolean remiseEnEtagere();
    
    public void setDisponibilite(boolean a);
    
    public String getNom();
}