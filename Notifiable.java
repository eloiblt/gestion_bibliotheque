public interface Notifiable 
{
	public void DocDisponible(Empruntable doc);
	
	public String getNom();
	
	public void setNbDocEmpruntes(int i);
	
	public boolean peutEmprunterAutreDocument();
}
