public abstract class MembreBibliotheque implements Notifiable
{
    protected String nom;
    protected String prenom;
    protected String numTel;
    protected String ads;
    protected int numAbo;
    protected int nbDocEmpruntes;
    protected static int dernierNumeroAbonne = -1;
    
    public MembreBibliotheque()
    {
        nom = "Inconnu";
        prenom = "Inconnu";
        numTel = "0";
        ads = "Inconnue";
        numAbo = ++ dernierNumeroAbonne;
        nbDocEmpruntes = 0;
    }

    public MembreBibliotheque(String prenom, String nom, String tel, String ads)
    {
        this.prenom = prenom;
        this.nom = nom;
        this.numTel = tel;
        this.ads = ads;
        numAbo = ++ dernierNumeroAbonne;
        nbDocEmpruntes = 0;
    }

    public String getNom()
    {
        return nom;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public String getNumTel()
    {
        return numTel;
    }

    public String getAds()
    {
        return ads;
    }

    public int getnumAbo()
    {
        return numAbo;
    }

    public int getNbDocEmpruntes()
    {
        return nbDocEmpruntes;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public void setNumTel(String tel)
    {
        this.numTel = tel;
    }

    public void setAds(String ads)
    {
        this.ads = ads;
    }

    public void setNom(int numAbo)
    {
        this.numAbo = numAbo;
    }

    public void setNbDocEmpruntes(int i)
    {
        nbDocEmpruntes += i;
    }

    public String toString()
	{
		return "\n--Informations du membre--\nNumero : " + numAbo + "\nPrenom et Nom : " + prenom + " " + nom.toUpperCase() + "\nAdresse : " + ads + "\nTelephone : " + numTel + "\nType : " + this.getClass().getName();
    }	
    
    public abstract void DocDisponible(Empruntable doc);
    
    public abstract boolean peutEmprunterAutreDocument();
}
