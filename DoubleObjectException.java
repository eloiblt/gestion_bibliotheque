public class DoubleObjectException extends Exception
{
	public DoubleObjectException (Object o)
	{
		System.out.println("");
        System.out.println("L'objet de type " + o.getClass() + "est deja present dans la liste, il ne peux donc pas etre ajoute");
        System.out.println("");
	}
}
