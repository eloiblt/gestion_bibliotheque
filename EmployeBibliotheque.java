public class EmployeBibliotheque implements Notifiable
{
	private String identification;
	private String nom;
	private int nbDocEmpruntes;
	
	public EmployeBibliotheque ()
	{
		this.identification = "";
		this.nom = "";
		this.nbDocEmpruntes = 0;
	}
	
	public EmployeBibliotheque(String identification, String nom)
	{
		this.identification = identification;
		this.nom = nom;
		this.nbDocEmpruntes = 0;
	}
	
	public String getIdentification()
	{
		return identification;
	}
	
	public String getNom()
	{
		return nom;
	}
	
	public int getNbDocEmpruntes()
	{
		return nbDocEmpruntes;
	}
	
	public void setIdentification(String identification)
	{
		this.identification = identification;
	}
	
	public void setNom(String nom)
	{
		this.nom = nom;
	}
	
	public void DocDisponible(Empruntable doc)
	{
		doc.setDisponibilite(false);
	}
	
	public void setNbDocEmpruntes(int i)
	{
	    nbDocEmpruntes += i;
	}
	
	public boolean peutEmprunterAutreDocument()
	{
		return true;
	}
}
