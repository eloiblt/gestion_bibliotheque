import java.util.ArrayList;

public class CatalogueBibliotheque
{
    private static ArrayList<DocBibliotheque> tab;
    private static int nbLivres = 0;
    private static int nbCD = 0;

    public CatalogueBibliotheque()
    {
        tab = new ArrayList<DocBibliotheque>();
    }

    public boolean ajouterDocument(DocBibliotheque doc) throws DoubleObjectException    
    {
    	try 
    	{
    		if (doc == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}
            
        for (int i = 0 ; i<tab.size() ; i++)
        {
            if (tab.get(i) == doc)
            {
                throw new DoubleObjectException(doc);
            }
        }

        if (doc.getClass().getName() == "Livre")
            nbLivres ++;
        if (doc.getClass().getName() == "CD")
            nbCD ++;

        tab.add(doc);
        System.out.println("Doc ajoute !");
        
        return true;
    }

    public boolean supprimerDocument(DocBibliotheque doc) throws NoneExistException
    {
    	try 
    	{
    		if (doc == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

        if (tab.indexOf(doc) == -1)
        {
            throw new NoneExistException (doc);
        }
        
        if (doc.getClass().getName() == "Livre")
            nbLivres --;
        if (doc.getClass().getName() == "CD")
            nbCD --;

        tab.remove(tab.indexOf(doc));
               
        System.out.println("Doc supprime !");
        return true;
    }

    public DocBibliotheque accesDocumentTab(int i) throws NoneExistException
    {
        if (i >= 0 && i < tab.size())           
            return tab.get(i);
        else
        {
            throw new NoneExistException();
        }  
    }

    public boolean emprunt(int i, Notifiable membre) throws NoneExistException, ErrorSeuilException, ErrorIndiceException
    {
    	if(!(tab.get(i) instanceof Empruntable))
            return false;

        if(i >= 0 && i < tab.size())
        {
            if (membre.peutEmprunterAutreDocument())             
            {
                if(((Empruntable) tab.get(i)).emprunt(membre))
                {
                    System.out.println("Doc emprunte !");
                    return true;
                }
                return false;
            }
            else
            {
                throw new ErrorSeuilException();
            }
        }
        else 
        {
            throw new ErrorIndiceException();
        }
    }
    
    public boolean retour(int i) throws ErrorIndiceException
    {
        if(!(tab.get(i) instanceof Empruntable))
            return false;

        if(i >= 0 && i<tab.size())
        {
            if (((Empruntable) tab.get(i)).retour())
            {
                System.out.println("Doc retourne !");
                return true;
            }
            else
            {
                System.out.println("Retour impossible !");
                return false;
            }
        }
        else
        {
            throw new ErrorIndiceException();
        }
    }
    
    public boolean reservation(int i, Notifiable membre) throws NoneExistException, ErrorIndiceException
    {
        if(!(tab.get(i) instanceof Empruntable))
            return false;

        if(i >= 0 && i<tab.size())
        {
            if (((Empruntable) tab.get(i)).reservation(membre))
            {
                System.out.println("Doc reserve !");
                return true;
            }
            else
            {
                System.out.println("Reservation impossible");
                return false;
            }
        }
        else
        {
            throw new ErrorIndiceException();
        }
    }

    public boolean annulerReservation(int i, Notifiable membre) throws NoneExistException, ErrorIndiceException
    {
        if(!(tab.get(i) instanceof Empruntable))
            return false;

        if(i >= 0 && i<tab.size())
        {
            if (((Empruntable) tab.get(i)).annulerReservation(membre))
            {
                System.out.println("Reservation annulee !");
                return true;
            }
            else
            {
                System.out.println("Annulation de reservation impossible");
                return false;
            }
        }
        else
        {
           throw new ErrorIndiceException();
        }
    }

    public void informationsDocumentsTab()
    {
        System.out.println("Le catalogue contient : ");
        for(int i = 0 ; i < tab.size() ; i++)
        {
            if(tab.get(i).getDisponibilite())
                System.out.println(i + " -- Titre : " + tab.get(i).getTitre() + ". Type : " + tab.get(i).getClass().getName() + ". Statut : " + tab.get(i).statut());
        }
    }

    public static ArrayList<DocBibliotheque> refDoc()
    {
        return tab;
    }
    
    public int afficheTailleTab()
    {
    	return tab.size();
    }

    public int compteLivres()
    {
        return nbLivres;
    }

    public int compteCDs()
    {
        return nbCD;
    }
}