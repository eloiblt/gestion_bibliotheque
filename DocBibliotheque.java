public abstract class DocBibliotheque 
{
	protected String codeArchivage;
	protected String titre;
	protected String auteurPpal;
	protected int anneePublication;
	protected String etat;  
	protected Notifiable emprunteur;
	protected Notifiable reserveur;
    public static int nbDocEmpruntes = 0;
    public static int nbDocPile = 0;
	public static int nbDocReservation = 0;
	protected boolean disponibilite = true;
	
	public DocBibliotheque ()
	{
		codeArchivage = "Code inconnu";
		titre = "Titre inconnu ";
		auteurPpal = "Auteur Inconnu";
		anneePublication = 0;
		etat = "etagere";
		emprunteur = null;
		reserveur = null;
	}

	public DocBibliotheque (String code, String titre, String auteur, int annee)
	{
		codeArchivage = code;
		this.titre = titre;
		auteurPpal = auteur;
		anneePublication = annee;
		etat ="etagere";
		emprunteur = null;
		reserveur = null;
	}
	
	public String getCodeArchivage()
	{
		return codeArchivage;
	}
	
	public String getTitre()
	{
		return titre;
	}
	
	public String getAuteur()
	{
		return auteurPpal;
	}
	
	public int getAnneePublication()
	{
		return anneePublication;
	}
	
	public String getEtat() 
	{
		return etat;
	}

    public Notifiable getEmprunteur()
    {
		
        return emprunteur;
	}
	
	public Notifiable getReserveur()
    {
        return reserveur;
    }
	
	public boolean getDisponibilite()
	{
		return disponibilite;
	}

	public void setCodeArchivage (String code)
	{
		codeArchivage = code;
	}
	
	public void setTitre (String titre)
	{
		this.titre = titre;
	}
	
	public void setAuteur (String auteur)
	{
		auteurPpal = auteur;
	}
	
	public void setAnneePublication (int annee)
	{
		anneePublication = annee;
	}
	
	public String statut()
	{
		String a = new String();
		String b = new String();
		switch(etat)
		{
			case "etagere":
				a = "Le document est dans les etageres";
				break;
			case "emprunte":
				a = "Le document est emprunte, par " + this.getEmprunteur().getNom();
				break;
			case "pileRetour":
				a = "Le document est dans la pile des retours";
				break;
			case "sectionReservation":
				a = "Le document est dans le bac des reservations";
				break;
		}

		if (reserveur != null)
			b = a + ", et est reserve par " + this.getReserveur();
		else 
			b = a + ", et n'est pas reserve";
		
		return b;
	}

	public String toString()
	{
		return "\n--Informations du document--\nCode : " + codeArchivage + "\nTitre : " + titre + "\nAuteur : " + auteurPpal + "\nAnnee de publication : " + anneePublication;
    }	   
}



