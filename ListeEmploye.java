import java.util.ArrayList;

public class ListeEmploye
{
    private static ArrayList<EmployeBibliotheque> tab;

    public ListeEmploye()
    {
        tab = new ArrayList<EmployeBibliotheque>();
    }

    public boolean ajouterEmploye(EmployeBibliotheque membre) throws DoubleObjectException
    {
    	try 
    	{
    		if (membre == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

        for (int i = 0 ; i < tab.size() ; i++)
        {
            if (tab.get(i) == membre)
            {
                throw new DoubleObjectException(membre);
            }
        }
        for (int i = 0 ; i < tab.size() ; i++)
        {
            if (tab.get(i) == membre)
            {
                throw new DoubleObjectException(membre);
            }
        }

        tab.add(membre);
        
        System.out.println("Membre ajoute !");
        return true;
    }

    public boolean supprimerEmploye(EmployeBibliotheque membre) throws DoubleObjectException
    {
    	try 
    	{
    		if (membre == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

        if (tab.indexOf(membre) == -1)
        {
            throw new DoubleObjectException(membre);
        }
        else
        {   
            tab.remove(tab.indexOf(membre));
            System.out.println("Membre supprime !");
            return true;
        }
    }

    public EmployeBibliotheque accesEmploye(int i) throws NoneExistException
    {
    	if(i >= 0 && i <= tab.size() - 1)
    		return tab.get(i);
    	else
        {
            throw new NoneExistException();
        }  
    }

    public void informationsEmploye()
    {
        System.out.println("Les employes sont : ");
        for(int i = 0; i<tab.size(); i++)
        {
            System.out.println(i + " -- " + tab.get(i).getNom());
        }
    }
    
    public int afficheTaille() 
    {
    	return tab.size();
    }

    public static ArrayList<EmployeBibliotheque> refEmploye()
    {
        return tab;
    }
}