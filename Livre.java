public class Livre extends DocBibliotheque implements Empruntable 
{
    private String editeur;
    private int nbPages;
    private String ISBN;

    public Livre()
    {
        super();
        editeur = "";
        nbPages = -1;
        ISBN = "";
    }

    public Livre(String code, String titre, String auteur, int annee, String editeur, int nbPages, String ISBN)
    {
        super(code, auteur, editeur, annee);
        this.editeur = editeur;
        this.nbPages = nbPages;
        this.ISBN = ISBN;
    }

    public String getEditeur()
    {
        return editeur;
    }

    public int getNbPages()
    {
        return nbPages;
    }

    public String getISBN()
    {
        return ISBN;
    }
    
    public String getNom()
    {
    	return super.getTitre();
    }
    
    public void setEditeur(String editeur)
    {
        this.editeur = editeur;
    }

    public void setNbPages(int nbPages)
    {
        this.nbPages = nbPages;
    }

    public void setISBN(String ISBN)
    {
        this.ISBN = ISBN;
    }
    
    public void setDisponibilite(boolean a)
	{
		disponibilite = a;
	}

    public String toString()
    {
        return super.toString() + "\n--Le document est un livre, voici ses informations specifiques : --" + "\nEditeur : " + editeur + "\nNombre de pages : " + nbPages + "\nISBN : " + ISBN;
    }

    public boolean emprunt(Notifiable emprunteur)
	{
		try 
    	{
    		if (emprunteur == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

		if (etat == "etagere")
		{
			etat = "emprunte";
			this.emprunteur = emprunteur;
			emprunteur.setNbDocEmpruntes(1);
			nbDocEmpruntes ++;
			return true;
		}

		else if (etat == "sectionReservation" && emprunteur == this.reserveur)
		{
			etat = "emprunte";
			this.emprunteur = emprunteur;
			emprunteur.setNbDocEmpruntes(1);
			reserveur = null;
			nbDocEmpruntes ++;
			nbDocReservation --;
			return true;
		}
		
		return false;		
    }	
    
    public boolean retour()
	{
		if (etat == "emprunte")
		{
            if (reserveur != null)
            {
				etat = "sectionReservation";
				emprunteur.setNbDocEmpruntes(-1);
				emprunteur = null;
				nbDocEmpruntes --;
                nbDocReservation ++;
                reserveur.DocDisponible(this);
			}
			
            else
            {
				etat = "pileRetour";
				emprunteur.setNbDocEmpruntes(-1);
				emprunteur = null;
				nbDocEmpruntes --;
                nbDocPile ++;
            }
                
			return true;
		}

		return false;		
    }	
    
    public boolean reservation(Notifiable reserveur)
	{
		try 
    	{
    		if (reserveur == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

		if (etat == "emprunte")
		{
			if(this.emprunteur != reserveur && this.reserveur == null)
			{
				this.reserveur = reserveur;
				return true;
			}			
		}
		
		return false;     			
	}
    
    public boolean annulerReservation(Notifiable membre) 
	{	
		try 
    	{
    		if (membre == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

		if (membre == this.reserveur)
		{
			if (etat == "emprunte" && this.reserveur != null)
			{
				reserveur = null;
				return true;
			}
		
			else if (etat == "sectionReservation")
			{
				reserveur = null;
				etat = "pileRetour";
				nbDocReservation --;
				nbDocPile ++;
			}
				
			return true;
		}

		return false;
	}
    
    public boolean remiseEnEtagere()
	{
		if (etat == "pileRetour")
		{
            etat = "etagere";
            nbDocPile --;
			return true; 
		}
		
		return false;  				
	}
}
