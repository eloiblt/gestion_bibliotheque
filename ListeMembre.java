import java.util.ArrayList;

public class ListeMembre
{
    private static ArrayList<MembreBibliotheque> tab;

    public ListeMembre()
    {
        tab = new ArrayList<MembreBibliotheque>();
    }

    public boolean ajouterMembre(MembreBibliotheque membre) throws DoubleObjectException
    {
    	try 
    	{
    		if (membre == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

        for (int i = 0 ; i < tab.size() ; i++)
        {
            if (tab.get(i) == membre)
            {
                throw new DoubleObjectException(membre);
            }
        }
        for (int i = 0 ; i < tab.size() ; i++)
        {
            if (tab.get(i) == membre)
            {
                throw new DoubleObjectException(membre);
            }
        }

        tab.add(membre);
        
        System.out.println("Membre ajoute !");
        return true;
    }

    public boolean supprimerMembre(MembreBibliotheque membre) throws DoubleObjectException
    {
    	try 
    	{
    		if (membre == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

        if (tab.indexOf(membre) == -1)
        {
            throw new DoubleObjectException(membre);
        }
        else
        {   
            tab.remove(tab.indexOf(membre));
            System.out.println("Membre supprime !");
            return true;
        }
    }

    public MembreBibliotheque accesMembre(int num) throws NoneExistException
    {
        for(int i=0 ; i<tab.size() ; i++)
        {
            if(tab.get(i).getnumAbo() == num)
                return tab.get(i);
        }
       
        throw new NoneExistException();
    }

    public void informationsMembres()
    {
        System.out.println("Les membres sont : ");
        for(int i = 0; i<tab.size(); i++)
        {
            System.out.println(i + " -- " + tab.get(i).getNom());
        }
    }
    
    public int afficheTaille() 
    {
    	return tab.size();
    }

    public static ArrayList<MembreBibliotheque> refMembre()
    {
        return tab;
    }
}