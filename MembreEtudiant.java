public class MembreEtudiant extends MembreBibliotheque
{
    private final int limite = 4;

    public MembreEtudiant()
    {
        super();
    }

    public MembreEtudiant(String prenom, String nom, String tel, String ads)
    {
        super(prenom, nom, tel, ads);
    }

    public boolean peutEmprunterAutreDocument()
    {
        if (nbDocEmpruntes < limite)
            return true;
        return false;
    }
    
    public void DocDisponible(Empruntable doc) 
    {
    	System.out.println("Le document " + doc.getNom() + " qui a �t� r�serv� par le membre du personnel " + super.getNom() + " est d�sormais disponible � l�emprunt au bureau des r�servations");
    }
}