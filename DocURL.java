public class DocURL extends DocBibliotheque
{
    private String URL;
    private String description;
    private String auteurEntreprise;

    public DocURL()
    {
        super();
        URL = "";
        description = "";
    }

    public DocURL(String code, String titre, String auteur, int annee, String url, String description, String entreprise)
    {
        super(code, titre, auteur, annee);
        URL = url;
        this.description = description;  
        if (auteur == "")
            this.auteurEntreprise = entreprise;
        else
        {
            System.out.println("Les champs auteur et entreprise sont remplis, entreprise sera mis a null");
            this.auteurEntreprise = "";
        }
    }

    public String getURL()
    {
        return URL;
    }

    public String getDescription()
    {
        return description;
    }

    public String getAuteurEntreprise()
    {
        return auteurEntreprise;
    }

    public void setURL(String url)
    {
        URL = url;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setAuteurEntreprise(String auteur)
    {
        if (this.getAuteur() == "")
            this.auteurEntreprise = auteur;
    }

    public boolean emprunt(MembreBibliotheque emprunteur)
	{
		System.out.println("Le document est un docURL, l'emprunt est donc impossible !");
		return false;		
	}	
    
    public String toString()
    {
        return super.toString() + "\n--Le document est un DocURL, voici ses informations specifiques : --" + "\nURL : " + URL + "\nDescription : " + description + "\nEntreprise auteure : " + auteurEntreprise;
    }
}