import java.util.InputMismatchException;
import java.util.Scanner;

public class MembrePersonnel extends MembreBibliotheque
{
    private final int limite = 8;

    public MembrePersonnel()
    {
        super();
    }

    public MembrePersonnel(String prenom, String nom, String tel, String ads)
    {
        super(prenom, nom, tel, ads);
    }

    public boolean peutEmprunterAutreDocument()
    {
        if (nbDocEmpruntes < limite)
            return true;
        return false;
    }
    
    public void DocDisponible(Empruntable doc)  
    {
    	Scanner input = new Scanner(System.in);
    	
    	int choix = 0;
    	while (true)
        {
            try
            {
            	System.out.println("Le document " + doc.getNom() + " est disponible\nSouhaitez vous le recuperer ?\n--1 Oui\n--2 Non");  
            	choix = input.nextInt();
                input.nextLine();
                if(choix < 1 || choix > 2)
                {
                    throw new ChoixHorsMenuException(choix);
                }
                System.out.println("\n");
                break;
            }
            catch (InputMismatchException exception)
            {
                input.nextLine();
                System.out.println("\nSaisie Invalide ! \n");
                continue;
            }
            catch (ChoixHorsMenuException exception2)
            {
                continue;
            }
        }
    	
    	if (choix == 1)
    	{
    		doc.emprunt(this);
    	}
    	else 
    	{
    		doc.remiseEnEtagere();
    	}
    }
}