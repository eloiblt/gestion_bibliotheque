public class CD extends DocBibliotheque implements Empruntable
{
    private String listeMorceaux;

    public CD()
    {
        super();
        listeMorceaux = "";
    }

    public CD(String code, String titre, String artiste, int annee, String liste)
    {
        super(code, titre, artiste, annee);
        listeMorceaux = liste;
    }

    public String getListeMorceaux()
    {
        return listeMorceaux;
    }
    
    public String getNom()
    {
    	return super.getTitre();
    }
    
    public void setListeMorceaux(String listeMorceaux)
    {
        this.listeMorceaux = listeMorceaux;
    }
    
    public void setDisponibilite(boolean a)
	{
		disponibilite = a;
	}

    public String toString()
    {
        return super.toString() + "\n--Le document est un CD, voici ses informations specifiques : --" + "\nListe des morceaux : " + listeMorceaux;
    }
    
    public boolean emprunt(Notifiable emprunteur)
	{
		try 
    	{
    		if (emprunteur == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

		if (etat == "etagere")
		{
			etat = "emprunte";
			this.emprunteur = emprunteur;
			emprunteur.setNbDocEmpruntes(1);
			nbDocEmpruntes ++;
			return true;
		}

		else if (etat == "sectionReservation" && emprunteur == this.reserveur)
		{
			etat = "emprunte";
			this.emprunteur = emprunteur;
			emprunteur.setNbDocEmpruntes(1);
			reserveur = null;
			nbDocEmpruntes ++;
			nbDocReservation --;
			return true;
		}
		
		return false;		/*pas empruntable si emprunte ou si pileretour*/
    }	
    
    public boolean retour()
	{
		if (etat == "emprunte")
		{
            if (reserveur != null)
            {
				etat = "sectionReservation";
				emprunteur.setNbDocEmpruntes(-1);
				emprunteur = null;
				nbDocEmpruntes --;
                nbDocReservation ++;
                reserveur.DocDisponible(this);
			}
			
            else
            {
				etat = "pileRetour";
				emprunteur.setNbDocEmpruntes(-1);
				emprunteur = null;
				nbDocEmpruntes --;
                nbDocPile ++;
            }
                
			return true;
		}

		return false;		/*pas retournable si etagere, pileretour, ou sectionReservation*/
    }
    
    public boolean emprunt(EmployeBibliotheque emprunteur)
	{
		try 
    	{
    		if (emprunteur == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

		if (etat == "etagere")
		{
			etat = "emprunte";
			this.emprunteur = emprunteur;
			nbDocEmpruntes ++;
			return true;
		}

		else if (etat == "sectionReservation" && emprunteur == this.reserveur)
		{
			etat = "emprunte";
			this.emprunteur = emprunteur;
			reserveur = null;
			nbDocEmpruntes ++;
			nbDocReservation --;
			return true;
		}
		
		return false;		/*pas empruntable si emprunte ou si pileretour*/
    }	
    
    public boolean reservation(Notifiable reserveur)
	{
		try 
    	{
    		if (reserveur == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

		if (etat == "emprunte")
		{
			if(this.emprunteur != reserveur && this.reserveur == null)
			{
				this.reserveur = reserveur;
				return true;
			}			
		}
		
		return false;     			/*pas reservable si etagere, pile retour ou sectionReservation*/
    }
    
    public boolean annulerReservation(Notifiable membre) 
	{	
		try 
    	{
    		if (membre == null)
    		{
    			throw new NullPointerException();
    		}
    	} 
    	catch (NullPointerException exception)
    	{
    		System.out.println("Object Null Erreur");
    		return false;
    	}

		if (membre == this.reserveur)
		{
			if (etat == "emprunte" && this.reserveur != null)
			{
				reserveur = null;
				return true;
			}
		
			else if (etat == "sectionReservation")
			{
				reserveur = null;
				etat = "pileRetour";
				nbDocReservation --;
				nbDocPile ++;
			}
				
			return true;
		}

		return false;
	}

    public boolean remiseEnEtagere()
	{
		if (etat == "pileRetour")
		{
            etat = "etagere";
            nbDocPile --;
			return true; 
		}
		
		return false;  				
	}
}